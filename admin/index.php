<?php
include_once("../core/config.php");	

echo <<<END
<!DOCTYPE html>
<html>
	<head>
		<title>Blog Template Admin</title>
		<link rel="stylesheet" href="../style.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<script src='http://code.jquery.com/jquery-2.1.4.min.js' type='text/javascript'></script>
		<script src='http://cdnjs.cloudflare.com/ajax/libs/autosize.js/1.18.4/jquery.autosize.min.js' type='text/javascript'></script>
	</head>
	<body>
	<script>
		$(document).ready(function(){
			$('textarea').autosize();
		});
	</script>
END;

if(!$loggedin){
echo <<<END
		<form method='post' autocomplete='off'>
			<input type='text' placeholder='Username' name='username' id='username'></input><br/>
			<input type='password' placeholder='Password' name='password' id='password'></input><br/>
			<button type='submit' id='submit'>Login</button>
		</form>
END;

	if(isset($_POST['username']) || isset($_POST['password'])){
		$error = "";
		$username = $_POST['username'];
		$password = $_POST['password'];
			
		if($username == "" || $password == ""){
			$error = "All fields must be filled.";
		} else {
			$result = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE username='$username'"));
				
			if(password_verify($password, $result[2])){
				$_SESSION['id'] = $result[0];
				$_SESSION['username'] = $username;
				$_SESSION['password'] = $password;
					
				$loggedin = TRUE;
					
				if($id != 1){
					echo "You don't have rights to get here.";
					header("Location: ../");
				} else {
					header("Location: ./");
				}
			} else {
				$error = "Invalid username or password";
			}
		}
		
		echo "<div id='error'>" . $error . "</div>";
	}
		
	echo $error;
} else if($_SESSION['id'] == 1){
	if(isset($_GET['logout'])){	
		session_destroy();	

		header("Location: ./");
	} else if(isset($_GET['delete'])){
		$id = $_GET['delete'];
		
		mysqli_query($connection, "DELETE FROM posts WHERE id='$id'");
		
		header("Location: ./");
	} else if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$post = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM posts WHERE id='$id'"));
		
echo <<<END
		<form method='post'>
			<input type='text' name='title' id='title' value='
END;
		echo $post[1];
echo <<<END
			'></input><br/>
			<textarea name='text' id='text'>
END;
		echo $post[2];
echo <<<END
			</textarea><br/>
			<button id='submit'>Save</button>
		</form>
END;
		
		if(isset($_POST['title']) || isset($_POST['text'])){
			$title = addslashes($_POST['title']);
			$text = addslashes($_POST['text']);
			$error = "";
			
			if($title == "" || $text == ""){
				$error = "All fields must be filled";	
			} else {			
				mysqli_query($connection, "UPDATE posts SET title='$title', text='$text' WHERE id='$id'");
				
				header("Location: ./");
			}
			
			echo "<div id='error'>" . $error . "</div>";
		}
	} else if(isset($_GET['newpost'])){
echo <<<END
		<form method='post' autocomplete='off'>
			<input type='text' name='title' id='title'></input><br/>
			<textarea name='text' id='text'></textarea><br/>
			<button id='submit'>Post</button>
		</form>
END;

		if(isset($_POST['title']) || isset($_POST['text'])){
			$title = addslashes($_POST['title']);
			$text = addslashes($_POST['text']);
			$error = "";
			
			if($title == "" || $text == ""){
				$error = "All fields must be filled";	
			} else {
				$id = mysqli_fetch_row(mysqli_query($connection, "SELECT MAX(id) AS id FROM posts"))[0] + 1;
				$date = time();
				
				mysqli_query($connection, "INSERT INTO posts VALUES('$id', '$title', '$text', '$date')");
				
				header("Location: ./");
			}
			
			echo "<div id='error'>" . $error . "</div>";
		}
	} else {
echo <<<END
		<div id='nav'>
			<a href='?newpost' id='nava'>New post</a>
			<a href='../' id='nava'>View site</a>
			<a href='?logout' id='nava'>Logout</a>
		</div>
END;
	}
} else {
	echo "You don't have rights to get here.";
	header("Location: ../");
}

echo <<<END
	</body>
</html>
END;

mysqli_close($connection);
?>
