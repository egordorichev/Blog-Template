<?php
session_start();
	
$mysql_host = "localhost";
$mysql_username = "user"; // Place your mysql username here 
$mysql_password = "password"; // Place your mysql password here 
$mysql_db = "bt";
$connection = mysqli_connect($mysql_host, $mysql_username, $mysql_password, $mysql_db);
$loggedin = FALSE;

if(mysqli_connect_errno()){
	die(mysqli_connect_error());
}

if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password'])){
	$loggedin = TRUE;
}

function emojis($text){
	return str_replace(
		array(
			'<3', 
			':)', 
			':(', 
			'[heart]', 
			'[smile]', 
			'[disappointed]', 
			'[computer]', 
			'[gun]', 
			'[dollar]', 
			'[gift]', 
			'[guitar]', 
			'[email]', 
			'[cool]', 
			'[iceream]', 
			'[id]', 
			'[iphone]', 
			'[key]', 
			'[octocat]', 
			'[clock]', 
			'[coffee]', 
			'[fire]', 
			'[new]', 
			'[phone]', 
			'[helicopter]', 
			'[+1]', 
			'[-1]',
			'[hamburger]',
			'[sos]',
			'[spruce]',
			'[diamond]',
			'[bomb]',
			'[house]',
			'[joystick]',
			'[pizza]',
			'[recycle]',
			'[apple]',
			'[sun]',
			'[boom]',
			'[sleeping]',
			'[plane]',
			'[clouds]'
		), 
		array(
			'<img src=\'images/assets/emojis/heart.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/smile.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/disappointed.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/heart.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/smile.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/disappointed.png\' id=\'emoji\'></img>', 
			'<img src=\'images/assets/emojis/computer.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/gun.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/dollar.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/gift.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/guitar.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/email.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/cool.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/icecream.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/id.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/iphone.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/key.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/octocat.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/clock.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/coffee.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/fire.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/new.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/phone.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/helicopter.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/+1.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/-1.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/hamburger.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sos.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/spruce.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/diamond.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/bomb.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/house.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/joystick.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/pizza.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/recycle.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/pizza.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/apple.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sun.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/boom.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/sleeping.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/plane.png\' id=\'emoji\'></img>',
			'<img src=\'images/assets/emojis/clouds.png\' id=\'emoji\'></img>'
		), $text);
}

?>
