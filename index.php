<?php
include_once("core/config.php");	
include_once("core/markdown.php");

echo <<<END
<!DOCTYPE html>
<html>
	<head>
		<title>Blog Template</title>
		<link rel="stylesheet" href="style.css">
		<script src='http://code.jquery.com/jquery-2.1.4.min.js' type='text/javascript'></script>
		<script src='http://cdnjs.cloudflare.com/ajax/libs/autosize.js/1.18.4/jquery.autosize.min.js' type='text/javascript'></script>
	</head>
	<body>
	<script>
		$(document).ready(function(){
			$('textarea').autosize();
		});
	</script>
END;

if(!$loggedin){
	if(isset($_GET['singup'])){
echo <<<END
		<form method='post' autocomplete='off'>
			<input type='text' placeholder='Username' name='username' id='username'></input><br/>
			<input type='password' placeholder='Password' name='password' id='password'></input><br/>
			<button id='submit'>Singup</button>
		</form>
END;

		if(isset($_POST['username']) || isset($_POST['password'])){
			$error = "";
			$username = $_POST['username'];
			$password = $_POST['password'];
			
			if($username == "" || $password == ""){
				$error = "All fields must be filled.";
			} else {
				$result = mysqli_query($connection, "SELECT * FROM users WHERE username='$username'");
				
				if(!mysqli_num_rows($result)){
					$password_hash = password_hash($password, PASSWORD_BCRYPT);
					$id = mysqli_fetch_row(mysqli_query($connection, "SELECT MAX(id) AS id FROM users"))[0] + 1;
					
					mysqli_query($connection, "INSERT INTO users VALUES('$id', '$username', '$password_hash')");
					
					$_SESSION['id'] = $id;
					$_SESSION['username'] = $username;
					$_SESSION['password'] = $password_hash;
					
					$loggedin = TRUE;
					
					header("Location: ./");
				} else {
					$error = "This username is already taken";
				}
			}
			
			echo "<div id='error'>" . $error . "</div>";
		}		
	} else if(isset($_GET['login'])){
echo <<<END
		<form method='post' autocomplete='off'>
			<input type='text' placeholder='Username' name='username' id='username'></input><br/>
			<input type='password' placeholder='Password' name='password' id='password'></input><br/>
			<button type='submit' id='submit'>Login</button>
		</form>
END;

		if(isset($_POST['username']) || isset($_POST['password'])){
			$error = "";
			$username = $_POST['username'];
			$password = $_POST['password'];
			
			if($username == "" || $password == ""){
				$error = "All fields must be filled.";
			} else {
				$result = mysqli_fetch_row(mysqli_query($connection, "SELECT * FROM users WHERE username='$username'"));
				
				if(password_verify($password, $result[2])){
					$_SESSION['id'] = $result[0];
					$_SESSION['username'] = $username;
					$_SESSION['password'] = $password;
					
					$loggedin = TRUE;
					
					header("Location: ./");
				} else {
					$error = "Invalid username or password";
				}
			}
			
			echo "<div id='error'>" . $error . "</div>";
		}
	} else {
echo <<<END
		<div id='nav'>
			<a href='?login' id='nava'>Login</a>
			<a href='?singup' id='nava'>Singup</a>
		</div>
END;
		$posts = mysqli_query($connection, "SELECT * FROM posts ORDER BY date DESC");
		$num = 10;
		
		if(mysqli_num_rows($posts) < 10){
			$num = mysqli_num_rows($posts);
		}
		
		for($i = 0; $i < $num; $i++){
			$post = mysqli_fetch_row($posts);
			
			echo "<div id='post'><h2>" . emojis($post[1]) . "</h2><i id='date'>" . date("M jS Y g:ia", $post[3]) . "</i><pre>" . emojis(Parsedown::instance()->text($post[2])) . "</pre></div>";
		}
		
		if(!$num){
			echo "There is no messages yet.";
		}
	}
} else {
	if(isset($_GET['logout'])){	
		session_destroy();	
		
		header("Location: ./");
	} else {
		echo "<div id='nav'>";
		
		if($_SESSION['id'] == 1){
			echo "<a href='admin/' id='nava'>Admin</a>";
		}
		
echo <<<END
			<a href='?logout' id='nava'>Logout</a>
		</div>
END;
		
		$posts = mysqli_query($connection, "SELECT * FROM posts ORDER BY date DESC");
		$num = 10;
		
		if(mysqli_num_rows($posts) < 10){
			$num = mysqli_num_rows($posts);
		}
		
		for($i = 0; $i < $num; $i++){
			$post = mysqli_fetch_row($posts);
			
			echo "<div id='post'>"; 
			
			if($_SESSION['id'] == 1){
				echo "<a href='admin/?delete=" . $post[0] . "' id='delete'></a> <a href='admin/?edit=" . $post[0] . "' id='edit'></a>";
			}
			
			echo "<h2>" . emojis($post[1]) . "</h2><i id='date'>" . date("M jS Y g:ia", $post[3]) . "</i><pre>" . emojis(Parsedown::instance()->text($post[2])) . "</pre></div>";
		}
		
		if(!$num){
			echo "There is no messages yet.";
		}
	}
}

echo <<<END
	</body>
</html>
END;

mysqli_close($connection);


?>
